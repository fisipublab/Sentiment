yang pertama harus didudukkan bahwa jabatan Gubernur dalam pandangan Islam dia diposisikan sebagai hakim.
apa itu hakim? Yakni a.. Satu kekuasaan satu yang memiliki kekuasaan   dia bisa melaksanakan, memiliki otoritas untuk melaksanakan suatu keputusan, dan Gubernur memiliki untuk itu.
maka gubernur dalam kategorisasi ini dia termasuk dalam kategori al-hakim.
dan salah satu syarat penting dari al-hakim adalah seorang muslim, haram hukumnya e mengangkat e seorang non muslim atau kafir menjadi gubernut, jika diangkat maka tidak sah.
di antara dalil yang digunakan oleh para ulama adalah surah An-nisa dalam ayat tersebut Allah SWT berfirman   dan sekali-kali Allah tidak menjadikan jalan bagi orang-orang kafir untuk menguasai orang-orang mukmin.
dalam ayat tersebut memang digunakan kalimat yang berbentuk khobar yang berarti informasi yang berarti nafi, jadi Allah menafikan a.. Sekali-kali Allah tidak menjadikan bagi orang-orang kafir jalan tetapi makna dari ayat ini adalah bukan nafil wujus tapi nafil jawas.
jadi bukan menafikan adanya tapi menafikan kebolehannya, a.. Berdasarkan kebolehannya.
diantaranya para ulama mengharamkan a.. Atau dengan kata lain tidak membolehkan, tidak sah seorang kafir untuk menjadi pemimpin bagi seorang muslim, bagi kaum muslimin.
nah, itu justru kesalahan dua kali, karena dalam pandangan Islam karena kepemimpinan muslim itu harus memenuhi dua hal; yang pertama adalah soal orangnya, sosok orang yang jadi pemimpinnya, dan yang kedua adalah aspek sistem yang diterapkan.
kedua-duanya harus tunduk kepada syariat Islam.
dari aspek orangnya, sepert yang tadi dikatakan bahwa diantara syaratnya adalah muslim dan syarat berikutnya berkaitan dengan sistem yang diterapkan.
itu adalah sistem Islam, nah ketika sekarang ini sistemnya bukan Islam dan lebih parah lagi manakala pemimpinnya bukan seorang muslim, maka ini pelanggaran dua kali.
dua hal dalam soal kepemimpinan dalam Islam.
haram hukumnya, bahkan seperti yang saya katakan, dia melakukan dua hal, jadi pada saat yang sama dia menerapkan sistem kufur dan orangnya orang kafir juga, jadi dua hal, gitu.
kita tidak melihat aspek siapa yang menunjuk, tapi yang dilihat adalah status gubernur itu al hakim atau bukan, ketika dia di posisi al hakim  , dia dalah seorang hakim.
dan dalam struktur pemerintah jelas, apalagi sekarang dalam e..apa istilahnya.. E.. Pilkada, maksud saya e.. Otonomi daerah, gubernur memiliki otoritas yang sangat luas, dia memiliki satu kebijakan, suatu keputusan dan keputusan itu bisa diterapkan tanpa ada pihak lain yang bisa menolak atau mencegahnya, ketika sekarang Ahok misalnya menginginkan itu di gusur, gusur! itu contoh.
bahkan ada sekian sekian banyak perda di pemerintahan daerah yang disitu adalah keputusan gubernur, maka itu menunjukkan bahwa gubernur adalah al hakim.
jadi memiliki keputusan dan memiliki otoritas menjalankan keputusan tersebut.
yang pertama saya tidak pernah mendengar penjelasan bahwa ada ulama yang berpandangan itu lebih baik, yang ada adalah e.. Suatu ungkapan yang itu dikutip oleh Ibnu naimiah, tapi itu bukan pendapat ibnu namiah sendiri.. Yang mengatakan bahwa pemerintahan bisa tegak walaupun kafir karena muhadil dan sebaliknya.
tapi kalau kita lihat ini bukan menunjukkan begitu lebih baik.
kalau pada takmiat itu berkaitan dengan pemimpin tidak ada perbedaan bahwa seorang pemimpin itu harus muslim, ini adalah perkara jiwa.
semua para ulama itu sepakat, tidak ada satupun yang khilaf dalam perkara ini. 
pentingnya keadilan tapi bukan berarti itu boleh, jadi tidak ada pilihan bahwa orang kafir itu jadi pemimpin.
dan sebenarnya kalau kita mau lebih jelas sebenarnya a.. Salah satu syarat seorang pemimpin ini adalah seorang yang adil.
adil adalah suatu lawan dari fasik, nah fasik itu apa? Fasik itu adalah orang yang gemar melakukan perbuatan-perbuatan dosa yang kecil atau dosa besar, jelas pernah melakukan al ghabir atau sering melakukan assawir maka dia termasuk seorang fasik.
nah, bagaimana kita bisa mengatakan orang kafir adil? Sulit sekali bagaimana dia terikat dalam hukum-hukum sawaf sehingga dia bisa mencapai apa yang disebut adil tersebut.
yang terbangun dalam opini bahwa dia jujur, dan seterusnya sudah dipatahkan oleh orang kasus sumber waras, itu menunjukkan betapa bahwa tidak bersih seperti yang diungkapkan orang bahwa dia itu orang bersih, dan seterusnya, saya kira itu propaganda jahat, ya, propaganda yang menyesatkan.
yang kedua yang justru penting adalah membandingkan Ahok dengan pemimpin-pemimpin muslim yang juga tidak tepat.
karena pembandingan itu tidak berarti membolehkan kalau ada orang Islam lebih buruk jual beli lalu kemudian orang kafir lebih baik, dan kemudian atas dasar itu boleh kemudian di angkat.
yang ketiga yang sangat penting lagi sebenarnya, adalah berkaitan dengan kejujuran, benarkan bahwa umat Islam sekarang ini sudah tidak ada seornag pun yang jujur? Sehingga harus mengemis pada orang kafir untuk menjadi pemimpin buat mereka.
saya kira ini pertanyaan penting, nah kalau  pemimpin yang sekarang ini a.. Tidak adil, yang dicalonkan dari umat Islam yang jujur, katakanlah calon-calon yang nampak bahwa mereka adalah orang-orang yang jujur, nah ini sebenarnya yang dikatakan bukan skandal orang, ini emmang bermasalah, bermula pada sistem yang diterapkan.
apabila sistem ini memberikan kesempatan muncul bagi orang-orang jahat, orang-orang  yang korup, orang-orang yang tidak adil, sementara mereka yang punya keterikatan kuat, pebgusaha, jangankan jujur, jujur itukan salah satu saja, terikat dengan syarat yang lain dan tidak muncul, dan tidak dimunculkan.
karena memang sistem sekarang ini memberikan kesempatan bagi orang sekarang ini untuk menjadi pemimpin.
oleh karena itu memang persoalannya bukan sekedar orang, tapi sistemnya.