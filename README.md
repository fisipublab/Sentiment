## ALIS 0.0.1

### Analisis Sentimen Bahasa Indonesia

Aplikasi pengolah sentimen untuk Bahasa Indonesia. Metode yang digunakan analisis frekuensi kata sifat yang terdapat dalam dokumne

### Kebutuhan Minimal
		
1. Python 2.7
2. Semua sistem operasi komputer yang mendukung python
3. Modul yang dibutuhkan dapat di-install memelalui pip install -r requirement.txt

### Penggunaan
1. Aplikasi terdapat pada folder Apps/TextAnalysis
2. Kemudian jalankan melalui terminal atau command line python Analyser.py --file --keyword
3. Hasilnya file berupa excel dengan rerata dan proporsi sentimen kata sifat dari seluruh dokumen
4. Jika terdapat maka proposi dan rerata kata sifat per keyword yang dibutuhkan

### TODO Next Step
1. Tersedia dalam versi web
2. Bisa menggunakan machine learning untuk pengolahan yang lebih komplek
3. Dapat difungsikan dengan hadoop
4. Untuk kata sifat bisa update secara otomatis jika terdapat kata sifat baru
5. API untuk output menjadi Grafik atau tampilan yang lebih menarik seperti menggunakan D3 js atau sejenisnya.