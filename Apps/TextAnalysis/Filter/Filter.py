import pandas as pd
import numpy as np

class Filter(object):
    def __init__(self):
        self.dictionary = None
    
    def tweet_order(self, tweets):
        ordered = {}

        for tweet, i in zip(tweets, range(0, len(tweets))):
            ordered[i] = tweet

        return ordered

    def keyword_filter(self, tweets, keywords):
        """
        :param tweets: string
        :param keyword: string
        :return:
        """
        temp = {}
        for keyword in keywords:
            help = []
            for tweet in tweets:
                if keyword.lower() in tweets[tweet]:
                    help.append(tweet)
            temp[keyword] = help

        return temp

    def adjective_groupby_keyword(self,adjective, clean_data, keyword):

        adjectives = []
        for adj in adjective:
            for data in clean_data[keyword]:
                if adj == data:
                    adjectives.append(adjective[adj])

        return adjectives

    def adjective_grouped(self, adjective, clean_data):
        # print clean_data
        adjectives = []
        for adj in adjective:
            for data in clean_data.itervalues():
                if adj == data:
                    adjectives.append(adjective[adj])
    
        return adjectives
    def groupby(self, clean_data, raw_data, adjectives, keyword):
                
        arrays = []
        anchor = []
        
        # count total tweet in keyword
        total_keyword = len(clean_data[keyword])
        temp = []

        for total in range(0,total_keyword):
            temp.append(keyword)

        temp3 = []
        for order in clean_data[keyword]:
            for data in raw_data:
                if order == data:
                    temp3.append(raw_data[data])

        anchor = [np.array(temp), np.array(clean_data[keyword])]

        df = pd.DataFrame(temp3, index=anchor)
        df = df.assign(adj=adjectives)

        return df

    def keyword_dataframe(self, index=None, tweets=None, adjective=None ):
        # df = pd.DataFrame(index, tweets)
        df1 = pd.DataFrame(np.array([
            ['a', 5, 9],
            ['b', 4, 61],
            ['c', 24, 9]]),
            columns=['name', 'attr11', 'attr12'])
        df2 = pd.DataFrame(np.array([
            ['a', 5, 19],
            ['b', 14, 16],
            ['c', 4, 9]]),
            columns=['name', 'attr21', 'attr22'])
        df3 = pd.DataFrame(np.array([
            ['a', 15, 49],
            ['b', 4, 36],
            ['c', 14, 9]]),
            columns=['name', 'attr31', 'attr32'])
        df = pd.merge(pd.merge(df1, df2, on='name'), df3, on='name')
        # df1 = pd.DataFrame(tweets['Gangguan mental'], columns=['tweet'])
        # df1 = df1.assign(e=['aku makan nais','aku makan nais','aku makan nais','aku makan nais','aku makan nais','aku makan nais','aku makan nais','aku makan nais','aku makan nais','aku makan nais'])
        # print df1