"""
    Split Document
    Input
    ex: ['data', 'data', 'data']
    Output
    Dict: E.x: {0: ['term', 'foo', 'baz'], 1: ['baz', 'foo', 'bar']}
"""
import re
def tokenizer(token_data):
    tokenize = {}
    for list_words, i in zip(token_data.values(), range(0, len(token_data))):
        # 1 tweet/1 video subtitle = 1 Document
        tokenize[i] = re.split(' ', list_words)
    return tokenize

