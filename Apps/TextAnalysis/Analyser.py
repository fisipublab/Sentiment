import os
from Dictionary.ArrayDictionary import ArrayDictionary
from Stemmer.Stemmer import Stemmer
from Helper.DocumentHelper import Helper
from Helper.TermsCount import TermsCount
from Helper.Writer import writer
from Filter import Tokenizer
from Cleaner import Cleaner
from Cleaner.Stopword import Stopword
from Filter.Filter import Filter
from pandas.io.json import json_normalize
from Sentiment.Sentiment import Sentiment
import pandas as pd
import numpy as np

class Analyser(object):

    def __init__(self, text=None):
        self.text = text
    
    def sentiment_analyser(self, isDev=False):
        # dictionary = ArrayDictionary(self.get_dict_lemma())
        tweet = self.get_json_difabel()
        adjectives = self.get_adjectives()        
        
        # stem = Stemmer().sastrawi(tweet)
        # clean =	Cleaner.cleansing(stem)
        cleans = Cleaner.cleansing(tweet)
        token = Tokenizer.tokenizer(cleans)

        # order tweet
        ordered = Filter().tweet_order(tweet)
        
        # get adjective
        # adjective_word = Stopword().stopword_removal(token, self.get_dict_lemma())
        
        # adjetives filter
        adjective_word =  Stopword().nonsentimen_removal(token, adjectives)
       
        # get keyword tweet
        filter_keyword = Filter().keyword_filter(cleans, self.get_keyword_difable())
        
        adjective_sentimen = Filter().adjective_grouped(adjective_word, filter_keyword)
        
        dataframes =  []
        dataframes_sentimen =  []
        all_term = []
        
        for keyword in self.get_keyword_difable():
            clean = Filter().adjective_groupby_keyword(adjective_word, filter_keyword, keyword)
            all_term.append(clean)
            total_term = Sentiment().term_counts(clean)
            emotion_label = Sentiment().term_label(adjectives, total_term)
            dataframe = pd.DataFrame(emotion_label.items(), columns=['adjective', 'sentiment'])
            dataframe = dataframe.sort_values(['sentiment'], ascending=['True'])
            sentimen = Sentiment().sentiment(dataframe, keyword)
            dataframes.append(dataframe)
            dataframes_sentimen.append(sentimen)

        writer_all = pd.ExcelWriter('Data mentah All Adjetives.xlsx', engine='xlsxwriter')

        for keyword, adjectives,sentimens in zip(self.get_keyword_difable(), dataframes, dataframes_sentimen):
            sheet = '%s' % keyword
            adjectives.to_excel(writer_all, sheet)
            sentimens.to_excel(writer_all, sheet_name=sheet, startcol=4, header=False, index=False)

        writer_all.save()

        # For Single File
        all_list = [item for sublist in all_term for item in sublist]
        total_term_single = Sentiment().term_counts(all_list)
        emotion_label_single = Sentiment().term_label(adjectives, total_term_single)
        dataframe_single = pd.DataFrame(emotion_label_single.items(), columns=['adjective', 'sentiment'])
        sentimen_single = Sentiment().sentiment(dataframe_single)
        writer = pd.ExcelWriter('Data mentah All Adjetives.xlsx', engine='xlsxwriter')
        sheet = 'sheet1'
        dataframe_single.to_excel(writer, sheet)
        sentimen_single.to_excel(writer, sheet_name=sheet, startcol=4, header=False, index=False)


        writer.save()

    
    def get_words_file(self):
        current_dir = os.path.dirname(os.path.realpath(__file__))
        dictionaryFile = current_dir + '/data/kamus/kata_dasar.txt'
            
        if not os.path.isfile(dictionaryFile):
            raise RuntimeError('Dictionary file is missing. It seems that your installation is corrupted.')
        
        dictionaryContent = ''
        with open(dictionaryFile, 'r') as f:
            dictionaryContent = f.read()
        
        return dictionaryContent.split('\n')
        
    def get_dict_lemma(self):
    
        dictionary = {}
        
        current_dir = os.path.dirname(os.path.realpath(__file__))
        dictionaryFile = current_dir + '/data/kamus/kamus_ivanlanin.txt'
        
        words = open(dictionaryFile, 'r')
        
        # kata dasar bahasa Indonesia
        for word in words:
            # explode row, 1st is class, 2nd is lemma
            attribute = word.lower().split('\t')
            if len(attribute) != 2:
                key = attribute[0].replace('', '')  # removing spaces if any
                key = key.rstrip('\n')
                # set to dictionary
                dictionary[key] = {'lemma': key, 'class': None}
            else:
                key = attribute[0].replace('', '')  # removing spaces if any
                # set to dictionary
                dictionary[key] = {'lemma': attribute[0], 'class': attribute[1].rstrip('\n')}
        return dictionary
    
    def get_keyword_difable(self):
        current_dir = os.path.dirname(os.path.realpath(__file__))
        dictionaryFile = current_dir + '/data/difable_keyword.txt'
    
        if not os.path.isfile(dictionaryFile):
            raise RuntimeError('Dictionary file is missing. It seems that your installation is corrupted.')
    
        dictionaryContent = ''
        with open(dictionaryFile, 'r') as f:
            dictionaryContent = f.read()

        return dictionaryContent.split('\n')

    def get_json_difabel(self):
        current_dir = os.path.dirname(os.path.realpath(__file__))
        # dictionaryFile = current_dir + '/data/difabel.json'
        dictionaryFile = current_dir + '/data/IndonesiaDisability.json'
        # data = 'D:/Project/Text-Analysis/Data/Difabel/IndonesiaDisability.json'
        
        help = Helper()
        data = help.open_json(dictionaryFile)
        result_df = json_normalize(data)

        # len(result_df.axes[1]) # count axes 1 / columns
        # result_df.reset_index().to_json(orient='records')
        # print result_df.axes
        documents = []

        df = pd.DataFrame()
        df['tweet'] =  result_df['tweet_text']
        df['user_name'] =  result_df['tweet_screen_name']

        remove_list = self.get_remove_list()
        df = Sentiment().remove_data(df, remove_list, 'user_name')
        # writer = pd.ExcelWriter('nonremoved_tweet.xlsx', engine='xlsxwriter')
        # df.to_excel(writer, 'sheet1')
        # writer.save()

        for v in df['tweet']:
            documents.append(v.encode("utf-8"))
        return documents

    def get_adjectives(self):
        current_dir = os.path.dirname(os.path.realpath(__file__))
        dictionaryFile = current_dir + '/data/kamus/kamus_kata_sifat.csv'
        dictionary = {}

        adjetives = pd.read_csv(dictionaryFile)
        for x in  range(0, len(adjetives)):
            dictionary[adjetives['lemma'][x]] = {'lemma': adjetives['lemma'][x], 'sentimen': adjetives['sentimen'][x]}
        return dictionary

    def get_remove_list(self):
        current_dir = os.path.dirname(os.path.realpath(__file__))
        dictionaryFile = current_dir + '/data/Usernamedrop.txt'
        dictionary = {}

        if not os.path.isfile(dictionaryFile):
            raise RuntimeError('Dictionary file is missing. It seems that your installation is corrupted.')

        dictionaryContent = ''
        with open(dictionaryFile, 'r') as f:
            dictionaryContent = f.read()

        return dictionaryContent.split('\n')

if __name__ == '__main__':
    Analyser().sentiment_analyser()