import os
import re

class Stopword(object):
    def __init__(self, dictionary=None):
        self.dictionary = dictionary

    """
        Stopword Removal
    """

    def non_root_removal(self, corpus_dict):
        clean_words = []
        unclean_words = {}
        stop_words = []

        # kata dasar bahasa Indonesia
        # kamus_data = '../Data/kamus/kata_dasar.txt'
        # kamus_kata_dasar = open(kamus_data, "r")
        ina_kamus_data = []
        # lines = kamus_kata_dasar.readlines()
        lines = self.dictionary
        for line in lines:
            line = line.strip('\n')
            ina_kamus_data.append(line)

        # removal
        counter = 0
        pemisah = " "
        for document in corpus_dict.values():
            kata_dasar = []
            non_baku = []
            for word in document:
                for stopword in ina_kamus_data:
                    if word == stopword:
                        kata_dasar.append(word)
                    else:
                        non_baku.append(word)
            unclean_words[counter] = non_baku
            clean_words.append(pemisah.join(kata_dasar))
            counter += 1

        return clean_words

    """
        Menghilangkan kata yang tidak penting
    """
    def stopword_removal(self, corpus_dict, dictionary):
        clean_words = {}
        pemisah = (' ')

        for term_list, x in zip(corpus_dict.itervalues(), range(0, len(corpus_dict))):
            helper = []
            # print term_list
            for term in term_list:
                for key in dictionary.values():
                    if key['lemma'] == term:
                        if key['class'] == 'pron':
                            pass
                        elif key['class'] == 'n':
                            pass
                        elif key['class'] == 'p':
                            pass
                        elif key['class'] == None:
                            pass
                        elif key['class'] == 'adv':
                            pass
                        elif key['class'] == 'num':
                            pass
                        elif key['class'] == 'v':
                            pass
                        else:
                            helper.append(term)
                            pass
            clean_words[x] = pemisah.join(helper)
        return clean_words
    
    def nonsentimen_removal(self, corpus_dict, dictionary):
        clean_words = {}
        pemisah = (' ')
        data = {}

        for term_list, x in zip(corpus_dict.itervalues(), range(0, len(corpus_dict))):
            helper = []
            # print term_list
            for term in term_list:
                for key in dictionary.values():
                    if key['lemma'] == term:
                        helper.append(key['lemma'])

            clean_words[x] = helper
        return clean_words
    
