"""
Clean dokumen from Unecessery like punctuation or etc
Input : List a document ex : ['lorem ipsum. Fernan ah id soget, mar to?', 'lorem opsum']
Output : Dict with List a document ex : {0:['lorem ipsum fernan ah id soget mar to'], 1: ['lorem opsum']
"""
import re
import string
def cleansing(documents):
    list_words = documents
    cleans = {}
    printable = set(string.printable)

    for i in  range(0, len(list_words)):
        list_words[i] = re.sub(r'http[s]?://(?:[a-z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-f][0-9a-f]))+','',list_words[i]) # URLs
        list_words[i] = re.sub(r'(?:\xe2\x80\x9c)','',list_words[i])
        list_words[i] = re.sub(r'(?:\xe2\x80\xa6)','',list_words[i])
        list_words[i] = re.sub(r'(?:\xe2\x80\x99)','',list_words[i])
        list_words[i] = re.sub(r'(?:\xe2\x80\x9c)','',list_words[i])
        list_words[i] = re.sub(r'(?:\xe2\x80\x98)','',list_words[i])
        list_words[i] = re.sub(r'(?:\xe2\x80\x93)','',list_words[i])
        list_words[i] = re.sub(r'(?:\xe2\x80\x9d)','',list_words[i])
        list_words[i] = re.sub(r'(?:\xf0\x9f\x91\x87)','',list_words[i])
        list_words[i] = re.sub(r'(?:\xc2|\xa0|\xf0)','',list_words[i])
        list_words[i] = re.sub(r'[^\x00-\x7F]+', ' ', list_words[i])
        list_words[i] = re.sub(r'(?<=\w)\.(?=\w)',' ',list_words[i])
        list_words[i] = re.sub(r'(?:!|\?|\.{3}|\.|\.\D|\.\s|\,|[\n]|\(|\)|\||\:)','', list_words[i]) #punctuation
        list_words[i] = re.sub(r'(?:[A-Z]+[\s]+@[\w_]+[:]|\s[:])','',list_words[i]) # Retweet
        list_words[i] = re.sub(r'(?:@[\w_]+)','',list_words[i]) # @-mention
        list_words[i] = re.sub(r'(?:\#+[\w_]+[\w\'_\-]*[\w_]+[\w:]|\s[:])','',list_words[i]) # hash-tags
        list_words[i] = re.sub(r'<[^>]+>','',list_words[i]) #html tags
        list_words[i] = list_words[i].lower()
        list_words[i] = re.sub(r'(?<=\w)  (?=\w)',' ', list_words[i])
        list_words[i] = re.sub(r'(?<=\w)\.\.(?=\w)',' ', list_words[i])
        list_words[i] = re.sub(r'(?<=\w)\,\.(?=\w)',' ', list_words[i])
        filter(lambda x: x in printable, list_words[i])
        cleans[i] = list_words[i]
    return cleans