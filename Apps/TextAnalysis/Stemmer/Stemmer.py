
from Sastrawi.Stemmer.StemmerFactory import StemmerFactory

class Stemmer(object):

    def __init__(self):
        pass

    def sastrawi(self, texts):

        stemmed = []
        factory =  StemmerFactory()
        machine =  factory.create_stemmer()

        for text in texts:
            stemmed.append(machine.stem(text))

        return stemmed
