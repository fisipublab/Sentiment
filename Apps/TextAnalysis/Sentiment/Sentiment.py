import numpy as np
import pandas as pd
class Sentiment(object):

    def __init__(self):
        pass

    def sentiment(self, dataframe, Keyword=None):
        temp =[]
        np.set_printoptions(precision=8)

        # for total in range(0,len(dataframe.index)):
        #     temp.append(Keyword)
        # anchor = np.array(temp)
        #
        # dataframe.reset_index()
        # dataframe.set_index(anchor)

        dataframe['sentiment'] = dataframe['sentiment'].astype(float)

        avg_positif =  dataframe[dataframe['sentiment'] > 0].mean()
        avg_negatif =  dataframe[dataframe['sentiment'] < 0].mean()
        
        total_positif = dataframe[dataframe['sentiment'] > 0].sum()
        total_negatif = dataframe[dataframe['sentiment'] < 0].sum()
        
        avg_negatif.sentiment = np.nan_to_num(avg_negatif.sentiment)
        avg_positif.sentiment =  np.nan_to_num(avg_positif.sentiment)

        total_positif.sentiment = np.nan_to_num(total_positif.sentiment)
        total_negatif.sentiment = np.nan_to_num(total_negatif.sentiment)

        total_frekuensi =  total_positif.sentiment + (total_negatif.sentiment * -1)

        proporsi_negatif = total_negatif.sentiment  / total_frekuensi
        proporsi_positif = total_positif.sentiment  / total_frekuensi

        proporsi_negatif = np.nan_to_num(proporsi_negatif) * 100.0 * -1
        proporsi_positif = np.nan_to_num(proporsi_positif) * 100.0
        
        total_proporsi = (avg_positif + avg_negatif) / 2.0

        Sentimen = {}
        Sentimen['Rerata Negatif'] = avg_negatif.sentiment
        Sentimen['Proporsi Negatif'] = proporsi_negatif
        Sentimen['Total Negatif'] = total_negatif.sentiment

        Sentimen['Rerata Positif'] = avg_positif.sentiment
        Sentimen['Proporsi Positif'] = proporsi_positif
        Sentimen['Total Positif'] = total_positif.sentiment

        Sentimen['Total Kata'] = total_frekuensi
        Sentimen['Rerata Total'] = total_positif.sentiment

        Sentimen = pd.DataFrame(Sentimen.items())
        return Sentimen
  
    def remove_data(self, dataframe, remove_list, column):
        by_column = '%s' % column
        cleaned = dataframe[~dataframe[by_column].isin(remove_list)]

        return cleaned

    def term_counts(self, documents):
        """
        Untuk menghitung total kata sifat didokumen.
        Dokumen dapat berupa seluruh dokumen maupun dokumen dengan keyword tertentu.
        :List documents:
        :return {'baik': 1, 'sehat': 1}:
        """
        data = {}
        for x in documents:
            for term in x:
                data[term] = data.get(term, 0) + 1
        return data

    def term_label(self, dictionary, data):
        """
        Melabeli kata sifat yang ditemukan dalam dokumen sesuai database sentimen yang telah ditemukan.
        :Contoh data
        {'buruk': 1, 'sehat': 1}

        :Return
        {'buruk': -1, 'sehat': 1}

        :Dict dictionary:
        :Dict data:
        :return Dict:
        """

        for term in data:
            for adj in dictionary.values():
                if adj['lemma'] == term:
                    if adj['sentimen'] == 1:
                        data[term] = data[term] * 1
                    elif adj['sentimen'] == -1:
                        data[term] = data[term] * -1
        return data