import csv
from collections import Counter
import time
import datetime
file_csv = 'D:/Project/Text-Analysis/Data/Conflict/attacks_data.csv'
attacks = []
with open(file_csv, 'rb') as f:
	reader = csv.reader(f)
	for read in reader:
		attacks.append(read)
"""
{
key : "negara",
values : "[[data]]"
}

1. Dari segi global jumlah korban meninggal dan korban luka timeline berdasarkan tahun
2. berdasar negara dan tahun
"""
negara = 0
values = []
values_data_2 = []
for items in attacks:
	values.append(items[1])
countries = {i:values.count(i) for i in values}
for country in countries:
	values_data = []
	for items in attacks:
		value = []
		if country in items[1]:
			value = [items[0], int(items[3])]
			values_data.append(value)
	# print values_data
	values_data_1 = {'key': country,'values' : values_data	}
	values_data_2.append(values_data_1)
# print values_data_2
import json
filename = 'global'
savefile = '../Data/' + filename + ".json"
try:
	jsondata = json.dumps(values_data_2,ensure_ascii=False)
	fd = open(savefile, 'w')
	fd.write(jsondata)
	fd.close()
except:
	print 'ERROR writing', filename