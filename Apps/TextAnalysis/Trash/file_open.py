import csv
import json
import re
from os import listdir
from os.path import isfile, join

class Open_file():
	"""docstring for writer"""
	def __init__(self):
		pass

	def in_folder(self, path):
		mypath = '/home/placebo/Documents/Apps/Data/IslamRadikal'
		onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]
		return onlyfiles

	"""Generators have a ``Yields`` section instead of a ``Returns`` section.

    Args:
        path : read file from folder path machine

    Yields:
        int: The next number in the range of 0 to `n` - 1.

    Examples:
        Examples should be written in doctest format, and should illustrate how
        to use the function.
		>>> print file_folder('home/jhondoe/examplefolder')
		['lorem ipsium	','lorem ipsum']
    """

	def file_folder(self, path):
		files = []
		doc = []
		space = (' ')
		open_file = []
		for filename in listdir(path):
			files.append(path+filename)
		for file in files:
			open_file = open(file, "r")
			helper = []
			for line in open_file:
				line = line.rstrip('\n')
				line = line.rstrip('\'')
				line = line.lower()
				helper.append(line)
			doc.append(space.join(helper))

		return doc

	def multi_file_(self, files, path):
		for file in files:
			open_file = open(path+file, "r")
		for line in open_file:
			print line

	def multi_Document(self, files):
		for file in files:
			print file
			# for line in file:
			# 	print line

	def csv_open(self,kamus):		
		# kamus_sentimen = '../Data/kamus/kamus_sentimen.csv'
		kamus_sentimens = []
		with open(kamus, 'rb') as f:
			reader = csv.reader(f)
			for row in reader:
				kamus_sentimens.append(row)
		return kamus_sentimens