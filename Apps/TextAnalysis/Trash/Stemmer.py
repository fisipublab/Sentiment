from Sastrawi.Stemmer.StemmerFactory import StemmerFactory

class Stemmer:

    def __init__(self):
        self.factory = StemmerFactory()
        self.stemmer_machine = self.factory.create_stemmer()

    def machine(self, documents):
        stemming = []

        for document in documents:
            stemming.append(self.stemmer_machine.stem(document))
        return stemming
