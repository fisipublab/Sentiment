class similarity(object):
	"""docstring for similarity"""
	def __init__(self, document):
        self.term_tf = {}
        self.term_idf = {}
        self.term_tfidf = {}

    """
        1 Dokumen = all tweet = all baris = all file video subtitle = 1 Buku = all status = 1 user
        Input From : self.term_total
        Format Input : Dict = {'term': word_total_in_a_document }
        ex : {'lorem' : 3,'ipsum':7,'etc':1.0,'lorem': 0.931231, 'opsum': 3,'etc':0}
        TF : term 't' in a document ex 100 word in a word and 3 term 't' in it
        So : term 't' /  total word in a document
        Format Output : Dict = {'term' : tf_value}
    """
    def tf(self):
        for term in document:
            self.term_tf[term] = document[term] / self.words_total

    """
        IDF :
    """
    def idf(self):
        for term in self.document_term:
            self.term_idf[term] = math.log((float(self.total_document)/self.document_term[term]),10)
        pass

    """
        Similarity
    """
    def similarities():
        self.term_idf[term] = math.log((float(self.total_document)/self.document_term[term]),10)
        pass

    """
        TFIDF
    """
    def tfidf(self):
        for __term_idf, __term_tf in zip(self.term_idf, self.term_tf):
            self.term_tfidf[__term_idf] =  self.term_idf[__term_idf] * self.term_tf[__term_tf]
        pass