import numpy as np
from numpy import linalg
import cvxopt

def linear_kernel(x1, x2):
	return np.dot(x1, x2)
	pass

class SVM(object):
	def __init__(self, kernel=linear_kernel, C=None):
		self.kernel = kernel
		self.C = C
		pass

	def fit(self, X, y):
		n_samples, n_features = X.shape
		K = np.zeros((n_samples, n_samples))
		for i in range(n_samples):
			for j in range(n_samples):
				K[i,j] = self.kernel(X[i], X[j])
		out = np.outer(y,y) * K
		## Bagian yang belum paham buat apa.
		P = cvxopt.matrix(out) #membentuk matrix dengan awal bilangan real
		q = cvxopt.matrix(np.ones(n_samples) * -1)
		A = cvxopt.matrix(y, (1,n_samples))
		b = cvxopt.matrix(0.0)

		if self.C is None:
			G = cvxopt.matrix(np.diag(np.ones(n_samples) * -1))
			print G
			h = cvxopt.matrix(np.zeros(n_samples))
			print h
		else:
			# matrix diagonal sperti identitas
			tmp1 = np.diag(np.ones(n_samples) * -1)
			# matrix identitas
			tmp2 = np.identity(n_samples)
			G = cvxopt.matrix(np.vstack((tmp1, tmp2))) #
			tmp1 = np.zeros(n_samples)
			tmp2 = np.ones(n_samples) * self.C
			h = cvxopt.matrix(np.hstack((tmp1, tmp2)))

		#solve Quadratic Programming
		solution = cvxopt.solvers.qp(P, q, G, h, A, b)

		# lagrange multipier
		a = np.ravel(solution['x'])
		print a

		# Support vectors have non zerp lagrange multipliers
		sv = a > 1e-5
		print sv
		ind = np.arange(len(a))[sv]
		print np.arange(len(a))[sv]
		self.a = a[sv]
		self.sv = X[sv]
		self.sv_y = y[sv]
		print self.a
		print self.sv
		print self.sv_y

		#intercept
		self.b = 0
		for n in range(len(self.a)):
			self.b += self.sv_y[n]
			self.b -= np.sum(self.a * self.sv_y * K[ind[n],sv])
		self.b /= len(self.a)

		# weight vector
		if self.kernel == linear_kernel:
			self.w = np.zeros(n_features)
			for n in range(len(self.a)):
				self.w += self.a[n] * self.sv_y[n] * self.sv[n]
		else:
			self.w = None

	def project(self, X):
		if self.w is not None:
			return np.dot(X, self.w) + self.b
		else:
			y_predict = np.zeros(len(X))
			for i in range(len(X)):
				s = 0
				for a, sv_y, sv in zip(self.a, self.sv_y, self.sv):
					s += a * sv_y * self.kernel(X[i],sv)
				y_predict[i] = s
			return y_predict + self.b

	def predict(self, X):
		return np.sign(self.project(X))

if __name__ == '__main__':
	import pylab as pl
	def plot_margin(X1_train, X2_train, clf):
			def f(x, w, b, c=0):
				# given x, return y such that [x,y] in on the line
				# w.x + b = c
				return (-w[0] * x - b + c) / w[1]

			pl.plot(X1_train[:,0], X1_train[:,1], "ro")
			pl.plot(X2_train[:,0], X2_train[:,1], "bo")
			pl.scatter(clf.sv[:,0], clf.sv[:,1], s=100, c="g")

			# w.x + b = 0
			a0 = -4; a1 = f(a0, clf.w, clf.b)
			b0 = 4; b1 = f(b0, clf.w, clf.b)
			pl.plot([a0,b0], [a1,b1], "k")

			# w.x + b = 1
			a0 = -4; a1 = f(a0, clf.w, clf.b, 1)
			b0 = 4; b1 = f(b0, clf.w, clf.b, 1)
			pl.plot([a0,b0], [a1,b1], "k--")

			# w.x + b = -1
			a0 = -4; a1 = f(a0, clf.w, clf.b, -1)
			b0 = 4; b1 = f(b0, clf.w, clf.b, -1)
			pl.plot([a0,b0], [a1,b1], "k--")

			pl.axis("tight")
			pl.show()

	def plot_contour(X1_train, X2_train, clf):
		pl.plot(X1_train[:,0], X1_train[:,1], "ro")
		pl.plot(X2_train[:,0], X2_train[:,1], "bo")
		pl.scatter(clf.sv[:,0], clf.sv[:,1], s=100, c="g")

		X1, X2 = np.meshgrid(np.linspace(-6,6,50), np.linspace(-6,6,50))
		X = np.array([[x1, x2] for x1, x2 in zip(np.ravel(X1), np.ravel(X2))])
		Z = clf.project(X).reshape(X1.shape)
		pl.contour(X1, X2, Z, [0.0], colors='k', linewidths=1, origin='lower')
		pl.contour(X1, X2, Z + 1, [0.0], colors='grey', linewidths=1, origin='lower')
		pl.contour(X1, X2, Z - 1, [0.0], colors='grey', linewidths=1, origin='lower')

		pl.axis("tight")
		pl.show()


	svm = SVM()
	x1_samples = np.array([[0.121, -0.2122],[0.9 , -0.77],[0.8, -0.5793],[0.5, -0.3412]])
	x2_samples = np.array([[-0.321123121, 0.52122],[-0.23397, 0.723477],[-0.832423, 0.423793],[0.42345, 0.432423]])
	# print x1_samples
	y1_samples = np.ones(len(x1_samples))
	print y1_samples
	y2_samples = np.ones(len(x2_samples)) * -1
	x1_test = np.array([[0.121, -0.2122],[0.9 , -0.77],[0.8, -0.5793],[0.5, -0.3412]])
	x2_test = np.array([[-0.321123121, 0.52122],[-0.23397, 0.723477],[-0.832423, 0.423793],[0.42345, 0.432423]])
	y1_test = np.ones(len(x1_samples))
	y2_test = np.ones(len(x2_samples)) * -1

	x_train = np.vstack((x1_samples, x2_samples)) # menggabungkan data secara vertikal
	y_train = np.hstack((y1_samples, y2_samples)) # horizontal
	x_cici = np.vstack((x1_test, x2_test)) # menggabungkan data secara vertikal
	y_cici = np.hstack((y1_test, y2_test)) # horizontal

	print y_train, "y_train"
	# x_train = x_samples[:5] # mengambil matrix sesuai angka

	# print x_train
	svm.fit(x_train, y_train)
	y_predict = svm.predict(x_train)
	correct = np.sum(y_predict == y_train)
	# print y_predict
	# print correct
	# print x_train[y_train==-1]
	# print x_train[y_train==1]
	plot_margin(x_train[y_train==1], x_train[y_train==-1], svm)
