class TermsCount(object):

    def term_counts_csv(self, corpus):
        data = {}
        data_persen = []
        term_csv = []

        total_document = float(0)
        for document in corpus.values():
            total_document += len(document)
        
        for document in corpus.values():
            for term in document:
                # menghitung jumlah kata dalam seluruh dokumen
                data[term] = data.get(term, 0) + 1
                counter = 0
                frequens = []
                if term in document:
                    counter += 1
                    frequens.append(term)
                    frequens.append(counter)
                    term_csv.append(frequens)
        
        term_persen = []
        for word in data:
            list_ = []
            persen = float(data[word]) / total_document * float(100)
            list_.append(word)
            list_.append(data[word])
            # list_.append(persen)
            term_persen.append(list_)
        
        return term_persen