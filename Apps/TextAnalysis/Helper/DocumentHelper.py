import json

class Helper(object):

    def __init__(self):
        pass

    def open_json(self, document):
        temp = []
        with open(document) as f:
            for line in f:
                # line = line.replace('\\','')
                try:
                   temp.append(json.loads(line))
                except ValueError:
                    print("data was not valid JSON")
        return temp